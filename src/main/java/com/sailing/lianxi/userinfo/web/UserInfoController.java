package com.sailing.lianxi.userinfo.web;

import com.sailing.lianxi.userinfo.entity.UserInfo;
import com.sailing.lianxi.userinfo.service.IUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @Auther: Administrator
 * @Date: 2018/10/5 19:13
 * @Description:
 */

@RestController
@RequestMapping(value = "/userinfo")
public class UserInfoController {

    @Autowired
    private IUserInfoService userInfoService;

    @RequestMapping("findUserInfoList")
    public List<UserInfo> findList(){
        return userInfoService.findUserInfoList();
    }

    @RequestMapping("insertUserInfo")
    public int insertUserInfo(UserInfo userInfo) {
        UserInfo userInfo1 = new UserInfo();
        userInfo.setUserName("003");
        userInfo.setCreater("wanggang");
        userInfo.setCreateTime(new Date());
        userInfo.setCreater("lili");
        userInfo.setUpdateTime(new Date());
        return userInfoService.insertUserInfo(userInfo);
    }

    @RequestMapping("updateUserInfo")
    public int updateUserInfo(UserInfo userInfo) {
        userInfo.setUid("001");
        userInfo.setUserName("003");
        userInfo.setAge(100);
        userInfo.setUpdater("lihou");

        userInfo.setUpdateTime(new Date());
        return userInfoService.updateUserInfo(userInfo);
    }

    @RequestMapping("deleteById")
    public int deleteById(String id) {
        id="b80a21e0c8af11e88882708bcda61ba6";
        return userInfoService.deleteById(id);
    }
}
