package com.sailing.lianxi.userinfo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.ibatis.javassist.runtime.Desc;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Auther: Administrator
 * @Date: 2018/10/5 19:14
 * @Description:
 */
@Data
public class UserInfo {

    private String uid;

    private String userName;

    private Integer age;

    private String passWord;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date  birth;

    private String creater;

    private Date createTime;

    private String updater;

    private Date updateTime;
}
