package com.sailing.lianxi.userinfo.service;

import com.sailing.lianxi.userinfo.entity.UserInfo;
import com.sailing.lianxi.userinfo.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: Administrator
 * @Date: 2018/10/5 19:18
 * @Description:
 */
@Service
public class UserInfoServiceImpl  implements  IUserInfoService{

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public List<UserInfo> findUserInfoList() {
        return userInfoMapper.findUserInfoList();
    }

    @Override
    public int insertUserInfo(UserInfo userInfo) {
        return userInfoMapper.insertUserInfo(userInfo);
    }

    @Override
    public int updateUserInfo(UserInfo userInfo) {
        return userInfoMapper.updateUserInfo(userInfo);
    }

    @Override
    public int deleteById(String id) {
        return userInfoMapper.deleteById(id);
    }
}
