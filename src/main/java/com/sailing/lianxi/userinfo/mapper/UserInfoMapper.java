package com.sailing.lianxi.userinfo.mapper;
import com.sailing.lianxi.userinfo.entity.UserInfo;

import java.util.List;

/**
 * @Auther: Administrator
 * @Date: 2018/10/5 19:16
 * @Description:
 */
public interface UserInfoMapper {

    List<UserInfo> findUserInfoList();

    int insertUserInfo(UserInfo userInfo);

    int updateUserInfo(UserInfo userInfo);

    int deleteById(String id);
}
